//
//  IconTextField.swift
//  IwannaFramework
//
//  Created by Victor Alejandria on 11/26/16.
//  Copyright © 2016 ISMCenter. All rights reserved.
//

import UIKit

@IBDesignable
public class IconTextField: UITextField {
    
    @IBInspectable var icon : UIImage? {
        didSet {
            if self.icon != nil {
                self.leftViewMode = UITextFieldViewMode.always
                let iconImage = UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.height - 15, height: self.frame.height - 15))
                iconImage.contentMode = UIViewContentMode.scaleAspectFit
                iconImage.image = icon
                let paddingView = UIView.init(frame: CGRect.init(x: 8, y: 0, width: self.frame.height + 10, height: self.frame.height - 10))
                paddingView.addSubview(iconImage)
                iconImage.center = paddingView.convert(paddingView.center, from: paddingView.superview)
                self.leftView = paddingView
            }
        }
    }
    
    @IBInspectable var borderCornerRadius: CGFloat = 5 {
        didSet {
            self.layer.cornerRadius = borderCornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 5 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.white {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var placeHolderColor: UIColor = UIColor.white {
        didSet {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: placeHolderColor])
        }
    }
    
    #if !TARGET_INTERFACE_BUILDER
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    #endif
}
