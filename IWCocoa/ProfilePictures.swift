//
//  ProfilePictures.swift
//  IwannaFramework
//
//  Created by Victor Alejandria on 11/26/16.
//  Copyright © 2016 ISMCenter. All rights reserved.
//

import UIKit

@IBDesignable
open class ProfilePictures: UIImageView {
    
    var myInsets = UIEdgeInsets()
    
    @IBInspectable var border : UIColor = UIColor.white {
        didSet {
            self.layer.borderWidth = 3.0
            self.layer.borderColor = border.cgColor
        }
    }
    
    @IBInspectable var circular : Bool = true {
        didSet {
            if self.circular {
                self.layer.cornerRadius = self.frame.size.width / 2;
                self.clipsToBounds = true;
            } else {
                self.layer.cornerRadius = 10.0
           }
        }
    }
    
    #if !TARGET_INTERFACE_BUILDER
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    required override public init(image: UIImage?) {
        super.init(image: image)
   }
    #endif
}
