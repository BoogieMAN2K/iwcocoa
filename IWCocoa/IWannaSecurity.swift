//
//  Security.swift
//  EnarMed
//
//  Created by Victor Alejandria on 26/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import Foundation
import UIKit
import LocalAuthentication

public struct IWannaSecurity {
    public static func touchIDLogIn(viewController: UIViewController, withCompletionHandler completionHandler: @escaping () -> Void) {
        if LAContext().canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error:nil) {
            LAContext().evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics,
                                       localizedReason: "Iniciando sesión con Touch ID",
                                       reply: { (success : Bool, error : NSError? ) -> Void in
                                        DispatchQueue.main.async(execute: {
                                            if success {
                                                completionHandler()
                                            }
                                            
                                            if error != nil {
                                                var message : NSString
                                                var showAlert : Bool
                                                switch(error!.code) {
                                                case LAError.authenticationFailed.rawValue:
                                                    message = "Ha habido un problema verificando su identidad"
                                                    showAlert = true
                                                    break;
                                                case LAError.userCancel.rawValue:
                                                    message = "Se cancelo el inicio de sesión."
                                                    showAlert = true
                                                    break;
                                                case LAError.userFallback.rawValue:
                                                    message = "You pressed password."
                                                    showAlert = true
                                                    break;
                                                default:
                                                    showAlert = true
                                                    message = "Touch ID puede no estar configurado"
                                                    break;
                                                }
                                                
                                                let alertView = UIAlertController(title: "Error",
                                                                                  message: message as String, preferredStyle:.alert)
                                                let okAction = UIAlertAction(title: "Continuar", style: .default, handler: nil)
                                                alertView.addAction(okAction)
                                                if showAlert {
                                                    viewController.present(alertView, animated: true, completion: nil)
                                                }
                                            }
                                        })
                                        
                                        } as! (Bool, Error?) -> Void)
        } else {
            completionHandler()
        }
    }
}
