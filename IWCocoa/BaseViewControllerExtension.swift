//
//  BaseViewControllerExtension
//  IwannaFramework
//
//  Created by Victor Alejandria on 11/4/16.
//  Copyright © 2016 ISMCenter. All rights reserved.
//
import Foundation
import UIKit
import EZSwiftExtensions

extension BaseViewController {
    
    open func registerForAutoScroll<T: UITextField>(scrollView: UIScrollView, textFields: [T]) {
        
        self.internalScrollView = scrollView
        
        textFields.forEach { (textField) in
            textField.delegate = self
        }
        
        self.registerKeyboardNotifications()
        self.hideKeyboardWhenTappedAround()
    }
    
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardShown),
                                               name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardHidden),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func unRegisterForKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardShown(_ aNotification: NSNotification) {
		guard let activeField = activeField else { return }
        var info: [AnyHashable: Any]? = aNotification.userInfo
        let kbSize = (info?[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.size
		var bkgndRect: CGRect? = activeField.superview?.frame
        bkgndRect?.size.height += (kbSize.height)
        self.activeField.superview?.frame = bkgndRect!
        self.internalScrollView.setContentOffset(CGPoint(x: CGFloat(0.0), y: CGFloat((self.activeField.frame.origin.y + (bkgndRect?.origin.y)!) - kbSize.height)), animated: true)
    }
    
    @objc func keyboardHidden(_ aNotification: NSNotification) {
        let contentInsets = UIEdgeInsets.zero
        self.internalScrollView.contentInset = contentInsets
        self.internalScrollView.scrollIndicatorInsets = contentInsets
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeField = textField
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeField = nil
    }
}
