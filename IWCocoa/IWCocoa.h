//
//  IWCocoa.h
//  IWCocoa
//
//  Created by Victor Alejandria on 19/5/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for IWCocoa.
FOUNDATION_EXPORT double IWCocoaVersionNumber;

//! Project version string for IWCocoa.
FOUNDATION_EXPORT const unsigned char IWCocoaVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <IWCocoa/PublicHeader.h>

@interface KeychainWrapper : NSObject

- (void)setObject:(id)inObject forKey:(id)key;
- (id)objectForKey:(id)key;
- (void)writeToKeychain;

@end
