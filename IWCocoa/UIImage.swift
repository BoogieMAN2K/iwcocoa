//
//  UIImage+Extensions.swift
//  IwannaFramework
//
//  Created by Victor Alejandria on 1/9/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//

import UIKit

public extension UIImage {
    
    public func toBase64(size: CGSize) -> String {
        let base64String = UIImageJPEGRepresentation(resize(image: self, size: size), 0.5)?.base64EncodedString()
        return base64String!
    }
    
    public func fromBase64(imageData: String) -> UIImage {
        let dataDecode:NSData = NSData(base64Encoded: imageData, options:.ignoreUnknownCharacters)!
        return UIImage(data: dataDecode as Data)!
    }
    
    public func resize(image : UIImage, size: CGSize) -> UIImage
    {
        var actualImageHeight : CGFloat = image.size.height
        var actualImageWidth : CGFloat = image.size.width
        let maxHeight : CGFloat = size.height
        let maxWidth : CGFloat = size.width
        var imageRatio : CGFloat = actualImageWidth / actualImageHeight
        let maxRatio: CGFloat = maxWidth / maxHeight
        let imageCompressionQuality : CGFloat = 1
        
        if actualImageHeight > maxHeight || actualImageWidth > maxWidth
        {
            if imageRatio < maxRatio
            {
                imageRatio = maxHeight / actualImageHeight
                actualImageWidth = imageRatio * actualImageWidth
                actualImageHeight = maxHeight
            }
            else
            {
                if imageRatio > maxRatio
                {
                    imageRatio = maxWidth / actualImageWidth
                    actualImageHeight = imageRatio * actualImageHeight
                    actualImageWidth = maxWidth
                }
                else
                {
                    actualImageHeight = maxHeight
                    actualImageWidth = maxWidth
                }
            }
        }
        let compressedImage : CGRect = CGRect(x: 0.0 , y: 0.0 , width: actualImageWidth , height: actualImageHeight)
        UIGraphicsBeginImageContext(compressedImage.size)
        image.draw(in: compressedImage)
        let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData: NSData = UIImageJPEGRepresentation(img, imageCompressionQuality)! as NSData
        UIGraphicsEndImageContext()
        return UIImage(data: imageData as Data)!
    }
    
}
