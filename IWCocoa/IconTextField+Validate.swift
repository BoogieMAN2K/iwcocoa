//
//  UITextField+Validate.swift
//  IwannaFramework
//
//  Created by Victor Alejandria on 10/20/16.
//  Copyright © 2016 ISMCenter. All rights reserved.
//

import Foundation
import UIKit

public extension IconTextField {
    
    public func validateEmail() -> (isValid: Bool, message: String) {
        let result = !self.isValidEmail(value: self.text!)
        var message = ""
        if result {
            message = "Dirección de correo inválida"
            self.clipsToBounds = true
            self.layer.borderColor = UIColor.red.cgColor
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: placeHolderColor])
           self.shake()
        }
        
        return (!result, message)
    }
    
    public func validatePhoneNumber() -> (isValid: Bool, message: String) {
        let result = !self.isValidPhoneNumber(value: self.text!)
        var message = ""
        if result {
            message = "Número de teléfono inválido"
            self.clipsToBounds = true
            self.layer.borderColor = UIColor.red.cgColor
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: placeHolderColor])
            self.shake()
        }
        
        return (!result, message)
    }
    
    public func validatePassword() -> (isValid: Bool, message: String) {
        let result = !self.isValidPasswordFormat(value: self.text!)
        var message = ""
        if result {
            message = "Clave inválida"
            self.clipsToBounds = true
            self.layer.borderColor = UIColor.red.cgColor
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: placeHolderColor])
            self.shake()
        }
        
        return (!result, message)
    }
    
    public func validateEmpty() -> (isValid: Bool, message: String){
        
        var message = ""
        if self.isEmpty() {
            message = "Campo requerido"
            self.clipsToBounds = true
            self.layer.borderColor = UIColor.red.cgColor
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: placeHolderColor])
            self.shake()
        }
        
        return (self.isEmpty(), message)
    }
    
    private func isValidEmail(value: String) -> Bool {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: value)
        return result
    }
    
    private func isValidPhoneNumber(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00)|(\\*)|())[0-9]{3,14}((\\#)|())$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    private func isValidPasswordFormat(value: String) -> Bool {
        let PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", PASSWORD_REGEX)
        let result =  passwordTest.evaluate(with: value)
        return result
    }
    
    private func isEmpty() -> Bool {
        return !self.hasText
    }
    
}
