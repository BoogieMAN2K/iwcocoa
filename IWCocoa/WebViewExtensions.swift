//
//  WebViewExtensions.swift
//  IwannaFramework
//
//  Created by Victor Alejandria on 11/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import Foundation
import ReactiveKit
import Bond

extension ReactiveExtensions where Base: UIWebView {
    public var delegate: ProtocolProxy {
        return base.reactive.protocolProxy(for: UIWebViewDelegate.self, selector: NSSelectorFromString("setDelegate:"))
    }
}

extension UIWebView {
    
    open var pageLoaded: Signal<Bool, NoError> {
        return reactive.delegate.signal(for: #selector(UIWebViewDelegate.webViewDidStartLoad(_:))) { (subject: PublishSubject<Bool, NoError>, _: UIWebView) in
            subject.next(true)
        }
    }

    open var pageFinishLoaded: Signal<Bool, NoError> {
        return reactive.delegate.signal(for: #selector(UIWebViewDelegate.webViewDidFinishLoad(_:))) { (subject: PublishSubject<Bool, NoError>, _: UIWebView) in
            subject.next(true)
        }
    }

    open var pageLoading: Signal<Bool, NoError> {
        return Signal1<Bool>(producer: { observer in
            observer.next(self.isLoading)
            return NonDisposable.instance
        })
    }
}
