//
//  VerticalCenteredTextView.swift
//  IWCocoa
//
//  Created by Victor Alejandria on 17/6/17.
//  Copyright © 2017 Iwanna Digital. All rights reserved.
//

class VerticalCenteredTextView: UITextView {

	override init(frame: CGRect, textContainer: NSTextContainer?) {
		super.init(frame: frame, textContainer: textContainer)
		addContentSizeObserver()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		addContentSizeObserver()
	}

	private func addContentSizeObserver() {
		self.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
	}

	private func removeContentSizeObserver() {
		self.removeObserver(self, forKeyPath: "contentSize")
	}

	override func observeValue(forKeyPath keyPath: String?, of object: Any?,
	                           change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {

		var top = (bounds.size.height - contentSize.height * zoomScale) / 2.0
		top = top < 0.0 ? 0.0 : top
		(object as AnyObject).setContentOffset(CGPoint(x: contentOffset.x, y: -top), animated: false)

	}

	deinit {
		removeContentSizeObserver()
	}

}
