//
//  Constants.swift
//  Gaver
//
//  Created by Victor Alejandria on 25/4/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import Foundation

public struct IWannaGoogle {
    static let kGoogleMapsAPIKey = "AIzaSyBSlsn4bWBd1mxHwVkmTmlMRimCT5xI1zo"
}

public struct IWannaWebService {
    //MARK: General Server URL
    static let usingSandbox = true
    static let kServerBaseURL = "http://gaver.free.web.ve/web/app_dev.php/api/"
    static let kProductionBaseURL = "http://gaver.free.web.ve/web/app_dev.php/api/"
}

public struct IWannaPayment {
    
    static let kSandboxMerchantID = "monpfzzhjhfglccsaktx"
    static let kSandboxApiKey = "sk_f3a76384233f4d99ae6b8221a22bb7e8"
    
    static let kProductionMerchantID = "m7jcaqrflstmfkk0kgpt"
    static let kProductionApiKey = "pk_9a0beaf6bd004bfcbd70df4d9b8cd931"
    
}
