//
//  Utilities.swift
//  IwannaFramework
//
//  Created by Victor Alejandria on 1/31/17.
//  Copyright © 2017 ISMCenter. All rights reserved.
//

import Foundation
import UIKit

public struct Utilities {
    
    public static func secondsToHoursMinutesSeconds (seconds : Double) -> (hours: Int, minutes: Int, seconds: Int) {
        let (hr,  minf) = modf (seconds / 3600)
        let (min, secf) = modf (60 * minf)
        return (Int(hr), Int(min), Int(60 * secf))
    }
    
    public static func errorMessage(title: String, localizeDescription: String, localizeFailureReason: String) -> NSError {
        let userInfo: [NSObject : AnyObject] =
            [
                NSLocalizedDescriptionKey as NSObject :  NSLocalizedString("Error", value: localizeDescription, comment: "") as AnyObject,
                NSLocalizedFailureReasonErrorKey as NSObject : NSLocalizedString("Error", value: localizeFailureReason, comment: "") as AnyObject
        ]
        let error = NSError(domain: "ShiploopHttpResponseErrorDomain", code: 401, userInfo: userInfo as? [String : Any])
        
        return error
    }
    
    public static func alertMessage(viewController: UIViewController, title: String, message: String, withCompletionHandler completionHandler: @escaping () -> Void = { }) {
        let alertView = UIAlertController(title: title,
                                          message: message, preferredStyle:.alert)
        
        let okAction = UIAlertAction(title: "Continuar", style: .default)  { (action) in
            completionHandler()
        }
        alertView.addAction(okAction)
        viewController.present(alertView, animated: true, completion: nil)
    }
}
